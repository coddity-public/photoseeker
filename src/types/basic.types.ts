export interface m_photo {
	collection_id: number,
	picture_id: string,
	width: number,
	height: number,
	url: string,
	filtered_assets: string[],
	available_assets: string[]
}

export interface u_photo {
	id: string,
	width: number,
	height: number,
	urls: {
		regular: string
	}
}

export interface request {
	image: {
		source: {
			imageUri: string
		}
	},
	features: [
		{
			type: string,
			maxResults: number
		}
	]	
}

export interface label {
	description: string,
    score: number
}

export interface labelList {
	labelAnnotations: label[]
}

export interface labelRequest {
	requests: request[]
}

export interface analyse {
	filtered_collection_pictures: m_photo[],
	collection_pictures: m_photo[]
}