import { m_photo, u_photo, labelList, labelRequest, analyse } from '../types/basic.types';

export interface ServicePhoto {
    getCollectionsId: (theme: string) => Promise<number>,
    getCollectionsPhoto: (id: number) => Promise<u_photo[]>
}

export interface ServiceAnalyse {
    analysePhoto: (requesting: labelRequest) => Promise<labelList[]>
}

export interface ServiceSeeker {
    getPhoto: (theme: string) => Promise<m_photo[]>,
    analysePhoto: (filter: string[] | null, photoList: m_photo[]) => Promise<analyse | null>
}
