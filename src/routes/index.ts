"use strict";

import express from 'express';
import { m_photo, analyse } from '../types/basic.types';
import Unsplash from '../services/Unsplash'
import Vision from '../services/Vision'
import Seeker from '../services/PhotoSeeker'
import { ServicePhoto, ServiceAnalyse } from "../types/service.types";

const router = express.Router();

// Route GET /collection/seek?theme&filter
// theme: Theme de recherche pour une recuperer une collection sur Unsplash
// filter: Filtre pour trier les photos d Unsplash selon les resultats des labels envoyés par l API Vision
router.get("/collection/seek", async (req, res) => {

	let theme : string = req.query.theme;
	let tempfilter : string | null = req.query.filter;
	let filter : string[] | null = null;
	if (tempfilter) {
		filter = tempfilter.toLowerCase().split(',');
	}

	let ServiceUnsplash: ServicePhoto = new Unsplash()
	let ServiceVision: ServiceAnalyse = new Vision()
	let ServiceSeeker = new Seeker({photo: ServiceUnsplash, analyse: ServiceVision})

	let photoList: m_photo[] = await ServiceSeeker.getPhoto(theme);
	console.log('filter', filter)
	let responses: analyse | null = await ServiceSeeker.analysePhoto(filter, photoList)

	res.send({ value: responses }).status(200);
});

export default {
	router: router
}