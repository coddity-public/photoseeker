"use strict";

import config from '../../config/api_key';
import axios from 'axios';
import { m_photo } from '../types/basic.types';
import { ServicePhoto } from "../types/service.types";

// Id application Unsplash
const applicationId = config.unsplash_application_id;

// Header d authorisation pour les requetes sur l API Unsplash
const headers = {
	"Authorization": "Client-ID " + applicationId
}

export default class Unsplash implements ServicePhoto {

    getCollectionsId = async (theme: string) => {
        try {
            let res_collection = await axios.get('https://api.unsplash.com/search/collections?page=1&per_page=1&query=' + theme, {headers: headers})
            if (process.env.NODE_ENV != 'test') {
                console.log('Unsplash Collection Return:', res_collection)
            }
            return res_collection.data.results[0].id;
        } catch(error) {
            console.error(error)
            return 1
        }
    }

    getCollectionsPhoto = async (collectionId: number) => {
        let photoList : m_photo[] = [];

        if (!collectionId || typeof collectionId !== "number") {
            collectionId = 1;
        }
        try {
            let res_photo = await axios.get('https://api.unsplash.com/collections/' + collectionId + '/photos?page=1&per_page=10', {headers: headers})
            if (process.env.NODE_ENV != 'test') {
                console.log('Unsplash Photo Return:', res_photo)
            }
            return res_photo.data;
        } catch(error) {
            console.error(error)
            return []
        }
    }
}
