"use strict";

import config from '../../config/api_key';
import axios, { AxiosResponse } from 'axios';
import { labelList, labelRequest } from '../types/basic.types';
import { ServiceAnalyse } from "../types/service.types";

// Clef d Api Google Vision
const google_api_key = config.google_vision_api_key;

export default class Vision implements ServiceAnalyse {

    analysePhoto = async (requesting: labelRequest) => {

        let res_vision: AxiosResponse | null = null;
        try {
            res_vision = await axios.post('https://vision.googleapis.com/v1/images:annotate?key=' + google_api_key, requesting)
            console.log(res_vision)
            if (res_vision == null) {
                return [];
            } else {
                let labels: labelList[] = res_vision.data.responses
                return labels;
            }
        } catch(error) {
            console.error(error)
            return [];
        }
    } 
}