"use strict";

import { request, u_photo, m_photo, labelRequest, labelList, analyse } from '../types/basic.types';
import { ServicePhoto, ServiceAnalyse, ServiceSeeker } from "../types/service.types";


export default class PhotoSeeker implements ServiceSeeker {
    protected servicePhoto: ServicePhoto;
    protected serviceAnalyse: ServiceAnalyse;
    filter: string[] | null = null;

    constructor(services: {photo: ServicePhoto, analyse: ServiceAnalyse}) {
        this.servicePhoto = services.photo;
        this.serviceAnalyse = services.analyse;
    }

    getPhoto = async (theme: string) => {
        let photoList : m_photo[] = [];
        let id: number = await this.servicePhoto.getCollectionsId(theme);
        let list: u_photo[] = await this.servicePhoto.getCollectionsPhoto(id);

        list.forEach((element : u_photo) => {
            let obj = {
                collection_id: id,
                picture_id: element.id,
                width: element.width,
                height: element.height,
                url: element.urls.regular,
                filtered_assets: [],
                available_assets: []
            }
            photoList.push(obj)
        })
        return photoList
    }

    analysePhoto = async (filter: string[] | null, photoList: m_photo[]) => {
        this.filter = filter;
        if (!photoList || photoList.length == 0) {
            return null;
        }
        
        let requesting: labelRequest = {
            requests: []
        }
        
        photoList.forEach((element : m_photo) => {
            let newRequest : request = {
                image: {
                    source: {
                        imageUri: element.url
                    }
                },
                features: [
                    {
                        type: "LABEL_DETECTION",
                        maxResults: 10
                    }
                ]
            }
            requesting.requests.push(newRequest);
        })

        // Effectue la recuperation des labels uniquement si il y a des filtres
        let labels: labelList[] = []
        if (this.filter) {
            labels = await this.serviceAnalyse.analysePhoto(requesting);
        }

    	let responses: analyse = {
            filtered_collection_pictures: [],
            collection_pictures: []
        }

        let newPhotoList = photoList.map((element: m_photo, index: number) => {
            if (this.filter && labels.length > 0 && labels[index].labelAnnotations) {
                let label = labels[index].labelAnnotations
                if (process.env.NODE_ENV != 'test') {
                    console.log('Vision Labels:', label)
                }
                element.available_assets = label.reduce(this._getLabelWith80Prc, []);
                element.filtered_assets = element.available_assets.filter(this._getLabelSameAsFilter);
                return element
            } else {
                return element
            }
        })
    
        newPhotoList.forEach((element: m_photo) => {
            if (element.filtered_assets.length > 0) {
                responses.filtered_collection_pictures.push(element)
            } else {
                responses.collection_pictures.push(element)
            }
        })

        return responses;
    }

    // Prend un tableau et le renvois en y ajoutant un label si son score est superieur ou egale a 80%
    _getLabelWith80Prc = (tab: string[], label: {score: number, description: string}) => {
		if (label.score >= 0.80) {
			tab.push(label.description);
		}
		return tab
	}

    // Renvois les descriptions de label si cela match avec les filtres envoyé par le client
	_getLabelSameAsFilter = (description: string) => {
		if (this.filter && this.filter.indexOf(description.toLowerCase()) >= 0) {
			return description
		}
	}
}