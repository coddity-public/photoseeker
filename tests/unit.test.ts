import { describe } from "mocha";
import { expect } from 'chai';

import Seeker from '../src/services/PhotoSeeker';
import { ServicePhoto, ServiceAnalyse } from '../src/types/service.types';
import { m_photo, u_photo, labelList, labelRequest, analyse } from '../src/types/basic.types';

function simuleId() {
    return 123;
}

function simulePhoto() {
    let obj: u_photo = {
        id: 'a',
        width: 1000,
        height: 1000,
        urls: {
            regular: 'fakeurl'
        }
    };
    return [obj];
}

function simuleList() {
    let obj: labelList = {
        labelAnnotations: [{
            description: 'test85',
            score: 0.85
        }, {
            description: 'test80',
            score: 0.80
        }, {
            description: 'test70',
            score: 0.70
        }]
    };
    return [obj];
}

class TestSPhoto implements ServicePhoto {
    getCollectionsId = async (theme: string): Promise<number> => {
        return await simuleId();
    }

    getCollectionsPhoto = async (id: number): Promise<u_photo[]> => {
        return await simulePhoto();
    }
}

class TestSAnalyse implements ServiceAnalyse {
    analysePhoto = async (requesting: labelRequest): Promise<labelList[]> => {
        return await simuleList();
    }
}

describe("Test Unit", function() {
    describe("#class Seeker", function() {
        let TestPhoto = new TestSPhoto()
        let TestAnalyse = new TestSAnalyse()
        let Seek = new Seeker({photo: TestPhoto, analyse: TestAnalyse});
        let photo: m_photo[] = []
        it("getPhoto take a param and return a m_photo[]", async function() {
            Seek.getPhoto('city').then((ret: m_photo[]) => {
                photo = ret;
                expect(ret).to.have.length(1);
                expect(ret[0].collection_id).to.be.equal(123);
                expect(ret[0].picture_id).to.be.equal('a');
                expect(ret[0].url).to.be.equal('fakeurl');
            })
        });
        it("analysePhoto take a filter and photo list then return an analyse (with a filter that match)", async function() {
            Seek.analysePhoto(['test85'], photo).then((ret: analyse | null) => {
                if (ret) {
                    expect(ret.filtered_collection_pictures).to.have.length(1);
                    expect(ret.collection_pictures).to.have.length(0);
                    expect(ret.filtered_collection_pictures[0].filtered_assets).to.have.length(1); // test85
                    expect(ret.filtered_collection_pictures[0].available_assets).to.have.length(2); // test80 && test85
                }
            })
        });
        it("analysePhoto take a filter and photo list then return an analyse (with a filter that doesnt match)", async function() {
            Seek.analysePhoto(['test70'], photo).then((ret: analyse | null) => {
                if (ret) {
                    expect(ret.filtered_collection_pictures).to.have.length(0);
                    expect(ret.collection_pictures).to.have.length(1);
                    expect(ret.collection_pictures[0].filtered_assets).to.have.length(0);
                    expect(ret.collection_pictures[0].available_assets).to.have.length(2); // test80 && test85
                }
            })
        });
    });
});